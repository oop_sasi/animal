/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sasina.inheritance;

/**
 *
 * @author admin
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "White", 0);
        animal.speak();
        animal.walk();
        
        System.out.println("--------------------");
        
        Dog dang = new Dog("Dang", "Black&White");
        dang.speak();
        dang.walk();
        
        System.out.println("--------------------");
        
        Cat zero = new Cat("Zero", "Orange");
        zero.speak();
        zero.walk();
        
        System.out.println("--------------------");
        
        Duck som = new Duck("Som", "Yellow");
        som.speak();
        som.walk();
        som.fly();
        
        System.out.println("--------------------");
        
        Dog to = new Dog("To", "Brown");
        Dog mome = new Dog("Mome", "Black&White");
        Dog bat = new Dog("Bat", "Black&White");
        Duck gabgab = new Duck("GabGab", "Black");
        
        System.out.println("--------------------");
        
        System.out.println("Som is Animal: " + (som instanceof Animal));
        System.out.println("Som is Duck: " + (som instanceof Duck));
        System.out.println("Som is Cat: " + (som instanceof Object));
        System.out.println("Animal is Dog: " + (animal instanceof Dog));
        System.out.println("Animal is Cat: " + (animal instanceof Cat));
        System.out.println("Animal is Duck: " + (animal instanceof Duck));
        System.out.println("Animal is Animal: " + (animal instanceof Animal));
        System.out.println("Dang is Animal: " + (dang instanceof Animal));
        System.out.println("Dang is Dog: " + (dang instanceof Dog));
        System.out.println("To is Animal: " + (to instanceof Animal));
        System.out.println("To is Dog: " + (to instanceof Dog));
        System.out.println("Mome is Animal: " + (mome instanceof Animal));
        System.out.println("Mome is Dog: " + (mome instanceof Dog));
        System.out.println("Bat is Animal: " + (bat instanceof Animal));
        System.out.println("Bat is Dog: " + (bat instanceof Dog));
        System.out.println("Zero is Animal: " + (zero instanceof Animal));
        System.out.println("Zero is Duck: " + (zero instanceof Cat));
        System.out.println("GabGab is Animal: " + (gabgab instanceof Animal));
        System.out.println("GabGab is Duck: " + (gabgab instanceof Duck));
        
        System.out.println("--------------------");
        
        Animal[] animals = {dang, to, mome, bat, zero, som, gabgab};
        for(int i=0; i<animals.length; i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
            System.out.println("--------------------");
        }
    }
}
